/// @file    command.h    
/// @brief   命令包声明  
///  
/// 定义各种TCP命令包以及相关结构体  


#ifndef COMMAND_H_  
#define COMMAND_H_  

typedef unsigned char uint8_t;

static const int tcp_cmd_header_len = 8;
static uint8_t tcp_cmd_header[tcp_cmd_header_len] = { 0xAA,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xFF };  

static const int max_user_name_len = 20;


typedef enum _serverCMD{
    cmd_save_user_name,
    cmd_save_user_age,
}serverCMD;


typedef enum _returnCMD{
    DVS_RETURN_SUCCESS = 0,        
    DVS_RETURN_FAIL,               
    DVS_RETURN_TIMEOUT,            
    DVS_RETURN_INVLID_HEADER,      
    DVS_RETURN_INVLID_CMD,         
    DVS_RETURN_INVLID_PRM, 
}returnCMD;

// 1bytes aligning  
#pragma pack( push, 1 )  


// server packet from client  
typedef struct _serverPack{      
    uint8_t cmdHeader[tcp_cmd_header_len];    
    serverCMD server_cmd;    // command id   
    // cmd param  
    union{         
        struct{              
            char username[max_user_name_len];   
        }userName; 
         
        struct{  
           int userage;   
        }userAge;  
 
    }parameters;   
}serverPack;  

// return packet from server  
typedef struct _returnPack{  
    // cmd header  
    uint8_t cmdHeader[tcp_cmd_header_len];  
    returnCMD return_cmd;   
}returnPack; 

#pragma pack( pop )  

static const int server_pack_len = sizeof(serverPack);
static const int return_pack_len = sizeof(returnPack);  

#endif