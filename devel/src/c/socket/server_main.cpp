/// 实现tcp服务器端测试的代码  


#include <iostream>
using namespace std;


#ifdef __cplusplus
extern "C" {               // 告诉编译器下列代码要以C链接约定的模式进行链接
#endif


#include <stdio.h>   
#include "server.h"  
#include "serverIf.h"  

#ifdef __cplusplus
}
#endif

const int local_port = 8000;

int main(int argc, char **argv){
	count<<"tcp test start"<<endl;
	
	if(open_port(local_port) != 0){
		return -1;
	}
	
	close_port();
	count<<"close port"<<endl;
	return 0;
}