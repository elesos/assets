// @file    main.c    
/// @brief   tcp客户端测试代码  
///  
/// 实现 tcp客户端测试  

#include <iostream>
using namespace std;


#ifdef __cplusplus
extern "C" {               // 告诉编译器下列代码要以C链接约定的模式进行链接
#endif


#include <stdio.h>   
#include "client.h"  

#ifdef __cplusplus
}
#endif


const string local_ip = "127.0.0.1";
const string dest_ip  = "192.201.0.8";
const int dest_port = 8000;

int main(int argc, char **argv){
	int i = 0;
	cout<<"tcp test start"<<endl;
	
	if(connect_server(dest_ip, dest_port) != 0){
		return -1;		
	}
	
	serverPack sPack;
	sPack.serverCMD = CMD_SAVE_USER_AGE;
	sPack.Parameters.UserAge.userage = 20;
	
	if(send_cmd(sPack) == -1){
		count<<"send cmd failed"<<endl;
	}
	
	getchar();
	getchar();
	
	close_connect();
	
	count<<"tcp test pass"<<endl;
	return 0;
}


















